package org.parse.job;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.parse.model.Weather;
import org.parse.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ParseTask {
    @Autowired
    WeatherService weatherService;

    @Scheduled(fixedDelay = 10000)
    public void parseWeather(){
        String url = "https://yandex.ru/pogoda/astana";
        Connection connect = Jsoup.connect(url);
        connect.userAgent("Chrome");
        connect.timeout(5000);
        connect.referrer("https://www.google.ru/");
        try {
            Document document = connect.get();
            Elements hour = document.getElementsByClass("fact__hour-label");

            for (Element element1: hour){
                String clock = element1.ownText();
                if (!weatherService.isExist(clock) ){
                    Weather weather1 = new Weather();
                    weather1.setClock(clock);
                    weatherService.save(weather1);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
