package org.parse.controller;

import org.parse.model.Weather;
import org.parse.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WeatherController {
    @Autowired
    WeatherService weatherService;

    @GetMapping(value = "/weather")
    public List<Weather> getAllWeather(){
        return weatherService.getAllWeather();
    }
}
