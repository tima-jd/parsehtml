package org.parse.services;

import org.parse.model.Weather;
import org.parse.repository.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeatherServiceImpl implements WeatherService {
    @Autowired
    WeatherRepository weatherRepository;

    @Override
    public void save(Weather weather) {
        weatherRepository.save(weather);
    }

    @Override
    public boolean isExist(String weathertemp) {
        List<Weather> weathers = weatherRepository.findAll();
        for (Weather weather:weathers){
            if (weather.getClock().equals(weathertemp)){
                return true;
            } }
        return false;
    }

    @Override
    public List<Weather> getAllWeather() {
        return weatherRepository.findAll();
    }
}
