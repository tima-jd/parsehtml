package org.parse.services;

import org.parse.model.Weather;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WeatherService {
    public void save(Weather weather);
    public boolean isExist(String weathertemp);
    public List<Weather> getAllWeather();
}
